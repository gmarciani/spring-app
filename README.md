# SPRINGAPP

*Scaffolding for Spring-based web application*


## Build
The app building is provided by Apache Maven. To build the app you need to run

    $app> mvn clean package

If you want to skip tests:

    $app> mvn clean package -P skip-tests


## Usage
To run the app with Apache Maven, you need to run

    $app>java -jar target/springapp-1.0.jar [YOUR ARGUMENTS HERE]


## Authors
Giacomo Marciani, [gmarciani@acm.org](mailto:gmarciani@acm.org)


## License
The project is released under the [MIT License](https://opensource.org/licenses/MIT).
