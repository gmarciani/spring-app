/*
  The MIT License (MIT)

  Copyright (c) 2017 Giacomo Marciani

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:


  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.


  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */
package com.gmarciani.springapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.gmarciani.springapp.core.CoreController;
import com.gmarciani.springapp.model.SimpleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * The simple error controller.
 * @author Giacomo Marciani {@literal <gmarciani@acm.org>}
 * @since 1.0
 */
@RestController
public class ErrorController {

  /*
  @RequestMapping(value = "error", method = RequestMethod.GET)
  public ModelAndView renderErrorPage(HttpServletRequest httpRequest) {

    ModelAndView errorPage = new ModelAndView("error");

    int httpErrorCode = (Integer) httpRequest
        .getAttribute("javax.servlet.error.status_code");

    String errorMsg;
    switch (httpErrorCode) {
      case 400: {
        errorMsg = "Http Error Code: 400. Bad Request";
        break;
      }
      case 401: {
        errorMsg = "Http Error Code: 401. Unauthorized";
        break;
      }
      case 404: {
        errorMsg = "Http Error Code: 404. Resource not found";
        break;
      }
      case 500: {
        errorMsg = "Http Error Code: 500. Internal Server Error";
        break;
      }
      default:
        errorMsg = "No error message";
        break;
    }


    errorPage.addObject("errorCode", httpErrorCode);
    errorPage.addObject("errorMsg", errorMsg);
    return errorPage;
  }
  */

  @RequestMapping(value = "/test/error", method = RequestMethod.GET)
  public void testErrors() {
    throw new NullPointerException("This is an error simulation");
  }
}
