appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%date{HH:mm:ss.SSS} %-5level %logger %method - %msg%n"
    }
}

appender("FILE", FileAppender) {
    file = "log/testFile.log"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%date{HH:mm:ss.SSS} %-5level %logger - %msg%n"
    }
}

root(INFO, ["CONSOLE"])

logger("com.gmarciani", ALL)

logger("ch.qos.logback", OFF)
logger("javax", WARN)

logger("org.apache.catalina", WARN)
logger("org.apache.coyote", WARN)
logger("org.apache.tomcat", WARN)
logger("org.hibernate", WARN)
logger("org.springframework", WARN)
logger("sun.rmi", OFF)